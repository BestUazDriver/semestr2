package itis.socialtest;

import itis.socialtest.entities.Post;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

public class AnalyticsServiceImpl implements AnalyticsService {
    @Override
    public List<Post> findPostsByDate(List<Post> posts, String date) {

        return null;
    }

    @Override
    public String findMostPopularAuthorNickname(List<Post> posts) {
        for (int i=0;i< posts.size();i++){

        }
        Stream stream= posts.stream();
        Comparator<Post> postComparator = new Comparator<Post>() {
            @Override
            public int compare(Post o1, Post o2) {
                return o1.getLikesCount().compareTo(o2.getLikesCount());
            }
        };
        return stream.max(postComparator).toString();
    }

    @Override
    public Boolean checkPostsThatContainsSearchString(List<Post> posts, String searchString) {
        return posts.stream().
                noneMatch(post -> post.getContent().toLowerCase().contains(searchString.toLowerCase()));
    }

    @Override
    public List<Post> findAllPostsByAuthorNickname(List<Post> posts, String nick){
        long max=0;
        List<Post> postsContent = new ArrayList<>();
        for (int i=0; i< posts.size();i++){
            if (posts.get(i).getAuthor().getNickname()==nick){
                postsContent.add(posts.get(i));
            }
        }
        return postsContent;
    }
}
