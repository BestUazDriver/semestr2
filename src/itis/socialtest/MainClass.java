package itis.socialtest;


import itis.socialtest.entities.Author;
import itis.socialtest.entities.Post;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/*
 * В папке resources находятся два .csv файла.
 * Один содержит данные о постах в соцсети в следующем формате: Id автора, число лайков, дата, текст
 * Второй содержит данные о пользователях  - id, никнейм и дату рождения
 *
 * Напишите код, который превратит содержимое файлов в обьекты в package "entities"
 * и осуществите над ними следующие опреации:
 *
 * 1. Выведите в консоль все посты в читабельном виде, с информацией об авторе.
 * 2. Выведите все посты за сегодняшнюю дату
 * 3. Выведите все посты автора с ником "varlamov"
 * 4. Проверьте, содержит ли текст хотя бы одного поста слово "Россия"
 * 5. Выведите никнейм самого популярного автора (определять по сумме лайков на всех постах)
 *
 * Для выполнения заданий 2-5 используйте методы класса AnalyticsServiceImpl (которые вам нужно реализовать).
 *
 * Требования к реализации: все методы в AnalyticsService должны быть реализованы с использованием StreamApi.
 * Использование обычных циклов и дополнительных переменных приведет к снижению баллов, но допустимо.
 * Парсинг файлов и реализация методов оцениваются ОТДЕЛЬНО
 *
 *
 * */

public class MainClass {

    private List<Post> allPosts;

    private AnalyticsService analyticsService = new AnalyticsServiceImpl();

    public static void main(String[] args) throws IOException {
        new MainClass().run("PostDatabase.csv", "Authors");

    }
    private void run(String postsSourcePath, String authorsSourcePath) throws IOException{
        ArrayList<Author> authors = new ArrayList<>();
        ArrayList<Post> posts = new ArrayList<>();
        BufferedReader authorsReader = new BufferedReader(new FileReader(authorsSourcePath));
        BufferedReader postsReader = new BufferedReader(new FileReader(postsSourcePath));
        String line1;
        while ((line1 = authorsReader.readLine()) != null) {
            String[] atributes = line1.split(",");
            long id=Integer.parseInt(atributes[0]);
            authors.add(new Author(id,atributes[1],atributes[2]));
        }
        String line2;
        while ((line2 = postsReader.readLine()) != null) {
            String[] atributes = line2.split(",");
            if (atributes.length>4) {
                for (int i = 4; i < atributes.length; i++) {
                    while (atributes.length != 4) {
                        atributes[3] = atributes[3] + atributes[i];
                    }
                }
            }
            int id=Integer.parseInt(atributes[0]);
            posts.add(new Post(atributes[2],atributes[3],(long)Integer.parseInt(atributes[1]),authors.get(id)));
        }
        for (int i=0;i<posts.size();i++){
            System.out.println(posts.get(i).getContent());
        }
    }
}
